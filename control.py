#!/usr/bin/env python

from random import randint
import time

# tuples (cutoff, transition)
#
filters = [
        (2000, 2000),
        (2500, 2000),
        ]

fpresets = [
        24000000,
        89200000,
        1776000000,
        ]

class Control(object):
     
    def __init__(self, tb):
        self.tb = tb

    def set_freq(self, f):
        self.tb.set_freq(f)
        print('\nFrequency = {}'.format(self.tb.get_freq()))

    def set_filter(self, c, t):
        self.tb.set_cutoff(c)
        self.tb.set_transition(t)
        print('\nCutoff = {}'.format(self.tb.get_cutoff()))
        print('Transition = {}'.format(self.tb.get_transition()))

    def sleep(self):
        time.sleep(randint(5, 30))

    def retune(self):
        self.set_freq(self.random_freq())
        self.set_filter(self.random_filter())

    def random_freq(self):
        return randint(80000000, 800000000)

    def random_filter(self):
        c, t = filters(random(0, len(filters) - 1))

    def test(self):
        self.set_freq(91200000)
        for t in range(2000, 10000, 500):
            for c in range(2000, 25000, 500):
                self.set_filter(c, t)
                time.sleep(10)

    def show_settings(self):
        print('  Frequency: {:12,}'.format(self.tb.get_freq()))
        print('     Cuttof: {:12,}'.format(int(self.tb.get_cutoff())))
        print(' Transition: {:12,}'.format(int(self.tb.get_transition())))
        print('Sample Rate: {:12,}'.format(self.tb.get_samp_rate()))
        print('     Volume: {:12,}'.format(int(self.tb.get_volume() * 100)))
        return

    def interactive(self):
        print('\nType Q to exit/quit\n\n')
        while True:
            line = raw_input('#: ')
            cmd, arg = self.parse_line(line)
            if not self.run_command(cmd, arg):
                break
        return

    def parse_line(self, line):
        cmd = None
        arg = None
        if not line:
            return cmd, arg

        parts = line.split()
        cmd = parts[0].lower()

        if len(parts) >= 2:
            try:
                arg = int(parts[1])
            except ValueError:
                pass
        return cmd, arg

    def run_command(self, cmd, arg):
        if cmd == 'q':
            return False
        if cmd == 'f':
            if not arg:
                return True
            self.tb.set_freq(arg)
            return True
        if cmd == 'c':
            if not arg:
                return True
            self.tb.set_cutoff(float(arg))
            return True
        if cmd == 't':
            if not arg:
                return True
            self.tb.set_transition(float(arg))
            return True
        if cmd == 'v':
            if not arg:
                return True
            self.tb.set_volume(float(arg) / 100)
        self.show_settings()
        return True

    def performB(self):
        fstart =   24000000
        fend   = 1776000000
        olow   =    -120000
        ohigh  =     120000
        ostep  =       1000
        fstep  =  2 * ohigh

        self.tb.set_cutoff(1000)

        while True:
            # freq = fpresets[randint(0, len(fpresets) - 1)]
            freq = fstart
            # fend = freq + fstep * 250
            self.tb.set_freq(freq)

            # ~ 200 seconds per loop
            while freq < fend:
                # print('Tuned to {} Hz'.format(freq))
                # print('Sweeping from {} Hz to {} Hz in {} Hz steps'.format(freq -ohigh, freq + ohigh, ostep))
                self.tb.set_freq(freq)
                for offset in range(olow, ohigh + ostep, ostep):
                    self.tb.set_offset(offset)
                    time.sleep(0.5)

                freq += fstep
                self.tb.set_freq(freq)

    def performA(self):
        fstart =  44000000
        fend   = 800000000
        fstep  =     10000
        cstart =      1000
        cend   =    100000
        cstep  =      1000

        while True:
            # freq = fstart
            freq = fpresets[randint(0, len(fpresets) - 1)]
            fend = freq + fstep * 250
            self.tb.set_freq(freq)
            print('Tuner set to {}.  Tuning up to {}'.format(freq, fend))

            # 200 seconds per loop
            while freq < fend:
                self.tb.set_freq(freq)

                for cutoff in range(cstart, cend + cstep, cstep):
                    self.tb.set_cutoff(cutoff)
                    time.sleep(1)

                freq += fstep
                self.tb.set_freq(freq)

                for cutoff in range(cend, cstart - cstep, -cstep):
                    self.tb.set_cutoff(cutoff)
                    time.sleep(1)
                freq += fstep

    def run(self):
        try:
            self.performB()
        except KeyboardInterrupt:
            return

if __name__ == '__main__':
    pass
