# Radio for Resonating Sculpture

Resonating Sculpture is a series of art works by [Reni
Hofmüller](http://renitentia.mur.at/).

## Project Description

The sculpture is a personalized antenna built from copper tape on a sheet of
tulle.  A pineA64 based sdr running GnuRadio is attached to the antenna.
The receiver is a DVB-T usb stick with an RTL2832  chip.

## Usage

First you compile a python script from the rtl-am.grc source file.  In the
main() method of the resulting python script you add these two lines right
after the line tb.start():

 from control import Control
 Control(tb).run()

The class Control does all the run time fiddling with frequencies and
filtering signals from the receiver.

## Version 3

Resonating Sculpture 3 can be seen and heard at the [Austrian Cultural Forum
(ACF)](http://www.acflondon.org/) in London from 20 September 2017 to 17
November 2017 as part of the project [Unconscious
Archives](https://ua2017.unconscious-archives.org/).
